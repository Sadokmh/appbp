package com.example.sadokmm.appmaterial.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sadokmm.appmaterial.R;
import com.example.sadokmm.appmaterial.activities.ShopActivity;
import com.example.sadokmm.appmaterial.objects.Plan;
import com.example.sadokmm.appmaterial.objects.Shop;

import java.util.ArrayList;
import java.util.List;

import static com.example.sadokmm.appmaterial.activities.FirstMain.myListShop;

public class AdapterForListShop extends RecyclerView.Adapter<AdapterForListShop.ViewHolder> implements Filterable {


    //public static ArrayList<Shop> myListShop=new ArrayList<>();
    private LayoutInflater layoutInflater;
    private List<Shop> myListShopSearch;


    public AdapterForListShop(Context context) {
        layoutInflater=LayoutInflater.from(context);
        myListShopSearch=new ArrayList<>();
    }


    @NonNull
    @Override
    public AdapterForListShop.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=layoutInflater.inflate(R.layout.shop_show,viewGroup,false);
        AdapterForListShop.ViewHolder viewHolder=new AdapterForListShop.ViewHolder(view);
        return viewHolder;
    }



    @Override
    public void onBindViewHolder(@NonNull AdapterForListShop.ViewHolder viewHolder, int i) {

        final Shop currentShop=myListShopSearch.get(i);
        viewHolder.itemTitle.setText(currentShop.getNom());
         viewHolder.itemAdress.setText(currentShop.getAdresse());
        viewHolder.itemThumbnail.setImageBitmap(currentShop.getPhoto());


        final int position=viewHolder.getAdapterPosition();

        viewHolder.itemThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(layoutInflater.getContext(),ShopActivity.class);


                intent.putExtra("i",currentShop.getNom());



                layoutInflater.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return myListShopSearch.size();
    }

    public void setMyList(ArrayList<Shop> list){
        myListShop=list;
        myListShopSearch.addAll(myListShop);
        notifyItemRangeChanged(0,list.size());
    }

    @Override
    public Filter getFilter() {
        return exampleFilter;
    }

    private Filter exampleFilter=new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<Shop> filteredShopList=new ArrayList<>();
            if (charSequence == null || charSequence.length() == 0){
                filteredShopList.addAll(myListShop);
            } else {
                String filterPattern=charSequence.toString().toLowerCase().trim();

                for (Shop shop : myListShop){
                    if (shop.getNom().toLowerCase().contains(filterPattern)){
                        filteredShopList.add(shop);
                    }
                }
            }

            FilterResults results=new FilterResults();
            results.values = filteredShopList;
            return results;

        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

            myListShopSearch.clear();
            myListShopSearch.addAll((List<Shop>)filterResults.values);
            notifyDataSetChanged();
        }
    };


    //

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView itemTitle;
        private TextView itemAdress;
        private ImageView itemThumbnail;
        private ImageView planType;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            itemTitle=(TextView)itemView.findViewById(R.id.planT);
             itemAdress=(TextView)itemView.findViewById(R.id.shopCustomAdress);
            itemThumbnail=(ImageView)itemView.findViewById(R.id.shopCustomPhoto);

            // planType = (ImageView)itemView.findViewById(R.id.typePlan);

        }
    }
}
