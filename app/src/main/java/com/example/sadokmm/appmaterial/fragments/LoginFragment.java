package com.example.sadokmm.appmaterial.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.sadokmm.appmaterial.R;
import com.example.sadokmm.appmaterial.activities.MainActivity;

import static com.example.sadokmm.appmaterial.activities.FirstMain.MY_SP_FILE;
import static com.example.sadokmm.appmaterial.activities.FirstMain.listAdmin;

public class LoginFragment extends Fragment {


    private EditText pass,email;
    private TextView connexion,verifMdp,verifEmail;



    public LoginFragment() {



    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login, container, false);


        connexion=(TextView) view.findViewById(R.id.connexion);
        verifEmail=(TextView) view.findViewById(R.id.verifEmail);
        verifMdp=(TextView) view.findViewById(R.id.verifMdp);
        email=(EditText) view.findViewById(R.id.emailLogin);
        pass=(EditText) view.findViewById(R.id.passLogin);

        connexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                verifEmail.setText("");
                verifMdp.setText("");
                if (email.getText().toString().isEmpty() && pass.getText().toString().isEmpty()) {
                    verifEmail.setText("Veuiller saisir vos informations ");
                } else if (email.getText().toString().isEmpty()) {
                    verifEmail.setText("Veuillez saisir votre email ");
                } else if (email.getText().toString().isEmpty()) {
                    verifMdp.setText("Veuillez saisir votre mdp");
                } else {


                    for (int i = 0; i < listAdmin.size(); i++) {


                        if (listAdmin.get(i).getEmail().equals(email.getText().toString())) {
                            if (listAdmin.get(i).getPass().equals(pass.getText().toString())) {

                              //Shared Preferences to save connectUser
                                SharedPreferences.Editor editor=getActivity().getSharedPreferences(MY_SP_FILE, Context.MODE_PRIVATE).edit();
                                editor.putString("connectUser",listAdmin.get(i).getEmail());
                                editor.commit();

                                Intent intent = new Intent(getContext(), MainActivity.class);
                                intent.putExtra("admin", listAdmin.get(i).getEmail());
                                startActivity(intent);
                            } else {
                                verifMdp.setText("Mot de passe incorrect");
                                break;
                            }


                        }


                    }
                }
            }
        });




        return view;


    }

}
