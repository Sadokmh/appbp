package com.example.sadokmm.appmaterial.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sadokmm.appmaterial.R;
import com.example.sadokmm.appmaterial.activities.PlanActivity;
import com.example.sadokmm.appmaterial.objects.Plan;

import java.util.ArrayList;
import java.util.List;

import static com.example.sadokmm.appmaterial.activities.FirstMain.myListPlan;
import static com.example.sadokmm.appmaterial.activities.FirstMain.myListPlanDrink;

public class AdapterForListDrink  extends RecyclerView.Adapter<AdapterForListDrink.ViewHolder> implements Filterable {




        private List<Plan> myListPlanDrinkSearch;
        private LayoutInflater layoutInflater;


        public AdapterForListDrink(Context context) {
            layoutInflater=LayoutInflater.from(context);
            myListPlanDrinkSearch=new ArrayList<>();
        }

        @NonNull
        @Override
        public AdapterForListDrink.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view=layoutInflater.inflate(R.layout.test,viewGroup,false);
            AdapterForListDrink.ViewHolder viewHolder=new AdapterForListDrink.ViewHolder(view);
            return viewHolder;
        }



        @Override
        public void onBindViewHolder(@NonNull com.example.sadokmm.appmaterial.Adapters.AdapterForListDrink.ViewHolder viewHolder, int i) {

            final Plan plan=myListPlanDrinkSearch.get(i);
            viewHolder.itemTitle.setText(plan.getTitre());
            // viewHolder.itemDesc.setText(currentPersonne.getDescription());
            viewHolder.itemThumbnail.setImageBitmap(plan.getImage());

            if ( plan.getType().equals("BP") ) {
                viewHolder.planType.setImageResource(R.drawable.ic_bp2);
            }


            viewHolder.itemThumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(layoutInflater.getContext(),PlanActivity.class);

                    int position=0;

                    for (int i=0 ; i<myListPlan.size() ; i++){

                        if (myListPlan.get(i)==plan)
                            position=i;

                    }

                    intent.putExtra("i",position);

                    layoutInflater.getContext().startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return myListPlanDrinkSearch.size();
        }

        public void setMyList(ArrayList<Plan> list){
            myListPlanDrink=list;
            myListPlanDrinkSearch.addAll(myListPlanDrink);
            notifyItemRangeChanged(0,list.size());
        }

    @Override
    public Filter getFilter() {
        return exampleFilter;
    }

    private Filter exampleFilter=new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<Plan> filteredDrinkList=new ArrayList<>();
            if (charSequence == null || charSequence.length() == 0){
                filteredDrinkList.addAll(myListPlanDrink);
            } else {
                String filterPattern=charSequence.toString().toLowerCase().trim();

                for (Plan plan : myListPlanDrink){
                    if (plan.getTitre().toLowerCase().contains(filterPattern)){
                        filteredDrinkList.add(plan);
                    }
                }
            }

            FilterResults results=new FilterResults();
            results.values = filteredDrinkList;
            return results;

        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

            myListPlanDrinkSearch.clear();
            myListPlanDrinkSearch.addAll((List<Plan>)filterResults.values);
            notifyDataSetChanged();
        }
    };


    //

        static class ViewHolder extends RecyclerView.ViewHolder {

            private TextView itemTitle;
            private TextView itemDesc;
            private ImageView itemThumbnail;
            private ImageView planType;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);

                itemTitle=(TextView)itemView.findViewById(R.id.itemTitre1);
                // itemDesc=(TextView)itemView.findViewById(R.id.itemDesc);
                itemThumbnail=(ImageView)itemView.findViewById(R.id.itemThumbnail1);

                planType = (ImageView)itemView.findViewById(R.id.typePlanItemm);

            }
        }

        public void refresh(){
            myListPlanDrinkSearch.clear();
            myListPlanDrinkSearch.addAll(myListPlanDrink);
            notifyDataSetChanged();
        }
    }


