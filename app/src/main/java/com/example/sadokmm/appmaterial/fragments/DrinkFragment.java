package com.example.sadokmm.appmaterial.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.sadokmm.appmaterial.Adapters.AdapterForList;
import com.example.sadokmm.appmaterial.Adapters.AdapterForListDrink;
import com.example.sadokmm.appmaterial.R;
import com.example.sadokmm.appmaterial.objects.Plan;

import java.util.ArrayList;

import static com.example.sadokmm.appmaterial.activities.FirstMain.myListPlan;
import static com.example.sadokmm.appmaterial.activities.FirstMain.myListPlanDrink;
import static com.example.sadokmm.appmaterial.activities.MainActivity.admin;
import static com.example.sadokmm.appmaterial.activities.MainActivity.currentFragment;
import static com.example.sadokmm.appmaterial.activities.MainActivity.searchView;

public class DrinkFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    private RecyclerView mlisteItem;
    public static AdapterForListDrink adapterForListDrink;



    private FoodFragment.OnFragmentInteractionListener mListener;

    private SwipeRefreshLayout mySwipe;

    public  DrinkFragment () {

        currentFragment = 2;

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =inflater.inflate(R.layout.fragment_drink, container, false);

        adapterForListDrink=new AdapterForListDrink(getContext()) ;
        adapterForListDrink.setMyList(myListPlanDrink);
        mlisteItem=view.findViewById(R.id.listItemHits);
        mlisteItem.setLayoutManager(new GridLayoutManager(getContext(),2));

        mlisteItem.setAdapter(adapterForListDrink);


        //swipe
        mySwipe=(SwipeRefreshLayout)view.findViewById(R.id.mySwipe);
        mySwipe.setOnRefreshListener(this);




        return view;
    }




    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }





    @Override
    public void onRefresh() {
        Toast.makeText(getContext(),"Refresh successfully done", Toast.LENGTH_SHORT).show();
        adapterForListDrink.notifyDataSetChanged();
        mySwipe.setRefreshing(false);

    }


}
