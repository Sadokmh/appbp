package com.example.sadokmm.appmaterial.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.sadokmm.appmaterial.R;

import java.util.ArrayList;

public class AdapterImageGallery extends BaseAdapter {

    ArrayList<Bitmap> listv=new ArrayList<>();
    Context context;

    public AdapterImageGallery(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return listv.size();

    }

    @Override
    public Object getItem(int i) {
        return listv.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View gridView=view;

        if (gridView == null){
            LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            gridView = inflater.inflate(R.layout.custom_shop_gallery_item,null);

        }

        ImageView im=(ImageView)gridView.findViewById(R.id.itemPhoto);

        im.setImageBitmap(listv.get(i));

        return gridView;


    }

    public void setListv(ArrayList<Bitmap> listv) {
        this.listv = listv;
    }
}
