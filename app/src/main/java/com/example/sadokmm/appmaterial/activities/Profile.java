package com.example.sadokmm.appmaterial.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.sadokmm.appmaterial.Adapters.AdapterForListPlanAdmin;
import com.example.sadokmm.appmaterial.R;
import com.example.sadokmm.appmaterial.objects.Admin;
import com.example.sadokmm.appmaterial.objects.Plan;
import com.pkmmte.view.CircularImageView;

import java.util.ArrayList;

import static com.example.sadokmm.appmaterial.activities.FirstMain.listAdmin;
import static com.example.sadokmm.appmaterial.activities.MainActivity.admin;
import static com.example.sadokmm.appmaterial.activities.FirstMain.myListPlan;
import static com.example.sadokmm.appmaterial.activities.FirstMain.myListShop;

public class Profile extends AppCompatActivity {


    Toolbar toolbar;

    private CircularImageView image_profile;
    private TextView enregistrer,niveau,errorView,username,emailProfileUser,nbPoints;
    private EditText email_profile,mdpNew,mdpNewConfirm;
    private RecyclerView mlisteItem;
    private AdapterForListPlanAdmin adap;
    private Admin profile;
    private ImageView stars;

    private ArrayList<Plan> planListProfile=new ArrayList<>();


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profile);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Intent intent=getIntent();
        String type=intent.getExtras().getString("type");


        if (type.equals("profile")) {

        //afficher profile utilisateur


            //chercher les plans de profil
            String emailProfile=intent.getExtras().getString("email");
            for (int i=0;i<myListPlan.size();i++){
                if (myListPlan.get(i).getAdmin().equals(emailProfile)){
                    planListProfile.add(myListPlan.get(i));
                }
            }

            //récupérer le profil
            for (int i=0;i<listAdmin.size();i++){
                if (listAdmin.get(i).getEmail().equals(emailProfile)){
                    profile=listAdmin.get(i);
                }
            }

            //adapter install
            adap=new AdapterForListPlanAdmin(this);
            adap.setMyList(planListProfile);
            adap.setHasStableIds(true);

            //recycler view install
            mlisteItem=(RecyclerView) findViewById(R.id.planProfileList);
            GridLayoutManager layoutManager = new GridLayoutManager(this,2);
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            mlisteItem.setLayoutManager(layoutManager);




            mlisteItem.setAdapter(adap);

            image_profile=(CircularImageView)findViewById(R.id.image_profile);
            username=(TextView)findViewById(R.id.username);
            emailProfileUser=(TextView)findViewById(R.id.email_profileuser);
            nbPoints=(TextView)findViewById(R.id.nbPoints);
            niveau=(TextView)findViewById(R.id.niveau);
            stars=(ImageView)findViewById(R.id.stars);

            username.setText(profile.getUsername());
            image_profile.setImageBitmap(profile.getPhoto());
            emailProfileUser.setText(profile.getEmail());
            nbPoints.setText(String.valueOf(profile.getNiveau()) + " points");

            if(profile.getNiveau()<1000) {
                niveau.setText("Débutant");
                stars.setImageResource(R.drawable.nejma);
            }
            else if(profile.getNiveau()<2000) {
                niveau.setText("Intermédiaire");
                stars.setImageResource(R.drawable.zouznjoum);
            }
            else if(profile.getNiveau()<3000) {
                niveau.setText("Confiant");
                stars.setImageResource(R.drawable.tlethanjoum);
            }
            else if(profile.getNiveau()<4000) {
                niveau.setText("Gourmand");
                stars.setImageResource(R.drawable.arbaanjoum);
            }
            else {
                niveau.setText("Super Gourmand");
                stars.setImageResource(R.drawable.cinqetoiles);
            }

            getSupportActionBar().setTitle(profile.getUsername());


            image_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showImage(profile.getPhoto());
                }
            });

        }








        else {


        //afficher profile admin connecté

            setContentView(R.layout.activity_admin_profile);
            toolbar = (Toolbar) findViewById(R.id.app_bar);

            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            image_profile=(CircularImageView)findViewById(R.id.image_profile);
            email_profile=(EditText)findViewById(R.id.email_profile);
            mdpNew=(EditText)findViewById(R.id.mdpNew);
            mdpNewConfirm=(EditText)findViewById(R.id.mdpNewConfirm);
            enregistrer=(TextView)findViewById(R.id.enregistrer);
            errorView=(TextView)findViewById(R.id.errorView);
            username=(TextView)findViewById(R.id.username);

            image_profile.setImageBitmap(admin.getPhoto());
            email_profile.setText(admin.getEmail());
            username.setText(admin.getUsername());
            getSupportActionBar().setTitle(admin.getUsername());


            //chercher les plans de l'admin connecté
            for (int i=0;i<myListPlan.size();i++){
                if (myListPlan.get(i).getAdmin().equals(admin.getEmail())){
                    planListProfile.add(myListPlan.get(i));
                }
            }

            //adapter install
            adap=new AdapterForListPlanAdmin(this);
            adap.setMyList(planListProfile);
            adap.setHasStableIds(true);


            //recyclerview install
            mlisteItem=(RecyclerView) findViewById(R.id.planProfileList);
            GridLayoutManager layoutManager = new GridLayoutManager(this,2);
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            mlisteItem.setLayoutManager(layoutManager);




            mlisteItem.setAdapter(adap);



            enregistrer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String mdpnew=mdpNew.getText().toString();
                    String mdpconfirm=mdpNewConfirm.getText().toString();

                    if (mdpnew.equals(mdpconfirm)){
                        admin.setPass(mdpnew);
                    }
                    else {
                        errorView.setText("Vérifier les MDPs");
                    }
                }
            });


            image_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showImage(admin.getPhoto());
                }
            });

        }






    }


    //Afficher l'Image du Shop
    public void showImage(Bitmap i) {
        final Dialog builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        View view = getLayoutInflater().inflate(R.layout.image_layout, null);

        ImageView im=(ImageView)view.findViewById(R.id.myImgView);
        ImageView close=(ImageView)view.findViewById(R.id.closeBtn);

        im.setImageBitmap(i);

        View imageLayout=getLayoutInflater().inflate(R.layout.image_layout,null);

        builder.addContentView(view,new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.hide();
                builder.dismiss();
            }
        });

        builder.show();


    }
}
