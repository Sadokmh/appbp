package com.example.sadokmm.appmaterial.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.sadokmm.appmaterial.fragments.DrinkFragment;
import com.example.sadokmm.appmaterial.fragments.FoodFragment;
import com.example.sadokmm.appmaterial.fragments.LoungeFragment;

public class PageAdapter extends FragmentPagerAdapter {

    private int numOfTabs;
    private int currentFrag;

    public PageAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FoodFragment();
            case 1:
                return new DrinkFragment();
            case 2:
                return new LoungeFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return "Food";
            case 1:
                return "Drink";
            case 2:
                return "Shop";
            default:
                return null;
        }
    }


    public int getCurrentFrag() {
        return currentFrag;
    }

    public void setCurrentFrag(int currentFrag) {
        this.currentFrag = currentFrag;
    }
}