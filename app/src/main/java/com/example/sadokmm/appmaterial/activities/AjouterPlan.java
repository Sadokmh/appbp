package com.example.sadokmm.appmaterial.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.resource.bitmap.BitmapDecoder;
import com.bumptech.glide.load.resource.drawable.DrawableResource;
import com.example.sadokmm.appmaterial.R;
import com.example.sadokmm.appmaterial.objects.Plan;

import java.util.ArrayList;


import static com.example.sadokmm.appmaterial.activities.FirstMain.myListPlanDrink;
import static com.example.sadokmm.appmaterial.activities.FirstMain.myListPlanFood;
import static com.example.sadokmm.appmaterial.activities.MainActivity.admin;
import static com.example.sadokmm.appmaterial.activities.FirstMain.myListPlan;
import static com.example.sadokmm.appmaterial.activities.FirstMain.myListShop;
import static com.example.sadokmm.appmaterial.fragments.DrinkFragment.adapterForListDrink;
import static com.example.sadokmm.appmaterial.fragments.FoodFragment.adapterForList;


public class AjouterPlan extends AppCompatActivity {

    private TextView planType;
    private EditText planTitre,planDesc,planPrix;
    private RatingBar planRating;
    private Spinner spinnerPlan,spinnerPlanGenre;
    private String planLocal,planGenre;
    private Bitmap myNewImage;
    private ImageView planImageView;
    ArrayList<String> spinnerList=new ArrayList<>();
    ArrayList<String> spinnerGenreList=new ArrayList<>();

    //for opencam
    int id2=1;


    String planTitreText,planTypeText,planDescText,planLoclText;
    Bitmap planImage;
    double planPrixDouble,planRatingDouble;


    Toolbar toolbar;



    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajouter_plan);


        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        planTitre=(EditText)findViewById(R.id.planNameAjout);
        planDesc=(EditText)findViewById(R.id.planDescAjout);
        planPrix=(EditText)findViewById(R.id.planPrixAjout);
        planRating=(RatingBar) findViewById(R.id.planRatingAjout);
        spinnerPlan=(Spinner) findViewById(R.id.planSpinnerAjout);
        planImageView=(ImageView)findViewById(R.id.imagePlanAjout);
        spinnerPlanGenre=(Spinner)findViewById(R.id.planSpinnerGenre);




        remplirListeSpinner();
        ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,spinnerList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPlan.setAdapter(arrayAdapter);
        ArrayAdapter<String> arrayAdapter2=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,spinnerGenreList);
        arrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPlanGenre.setAdapter(arrayAdapter2);
        spinnerPlan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                planLocal=adapterView.getItemAtPosition(i).toString();
                //Toast.makeText(adapterView.getContext(), "Selected: " + planLocal, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerPlanGenre.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                planGenre=adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });




        Intent intent=getIntent();

        planTypeText=intent.getExtras().getString("type");

        if (planTypeText.equals("BP")){
            getSupportActionBar().setTitle("Ajouter un bon plan ");

        }
        else {
            getSupportActionBar().setTitle("Ajouter un mauvais plan");

        }



    }



    public void buCam(View view){

        opencam();
    }


    public void buAdd(View v){

        /*Intent resultIntent = new Intent();
        resultIntent.putExtra("name",name.getText().toString());
        resultIntent.putExtra("des",des.getText().toString());
        resultIntent.putExtra("av",myNewImage);
        setResult(Activity.RESULT_OK, resultIntent);*/

        planTitreText=planTitre.getText().toString();
        planDescText=planDesc.getText().toString();
        //planTypeText="bp";
        planLoclText=planLocal;
        planImage=myNewImage;


        Drawable dahd=new BitmapDrawable(getResources(),myNewImage);


        planPrixDouble=Double.parseDouble(planPrix.getText().toString());
        planRatingDouble=(double)planRating.getRating();



        int pts=100;
    //gamification
    //Chercher si l'admin a publié dans un nouveau endroit , si oui il va prendre 100 points si non 40
        for ( int i=0;i<myListPlan.size();i++){
            if (myListPlan.get(i).getAdmin().equals(admin.getEmail())){
                if (myListPlan.get(i).getLocalisation().equals(planLocal)){
                    pts=40;
                    break;
                }
            }
        }


        Plan newP=new Plan(planTitreText,planDescText, myNewImage,planTypeText,planPrixDouble,planRatingDouble,planLoclText,planGenre,admin.getEmail());
        myListPlan.add(newP);
     //gamification
        admin.setNiveau(admin.getNiveau()+pts);

        //Ajouter le plan au shop correspondant
        for(int i=0;i<myListShop.size();i++) {
            if (myListShop.get(i).getNom().equals(planLocal)){
                myListShop.get(i).getListPlan().add(newP);
            }
        }

        if (planGenre.equals("Food")) {
            myListPlanFood.add(newP);
            adapterForList.refresh();
        }
        else {
            myListPlanDrink.add(newP);
            adapterForListDrink.refresh();
        }





        finish();

    }




    //Remplir liste spinner par les noms des Shops

    public void remplirListeSpinner(){

        for(int i=0;i<myListShop.size();i++){

            spinnerList.add(myListShop.get(i).getNom());


        }

        spinnerGenreList.add("Food");
        spinnerGenreList.add("Drink");


    }








    void opencam() {

        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, id2);
            }

        }
        catch (Exception e){
            Toast.makeText(this,e.toString(),Toast.LENGTH_LONG).show();

        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == id2 && resultCode == RESULT_OK)
        {

            Bundle extras = data.getExtras();
            myNewImage = (Bitmap) extras.get("data");
            planImageView.setImageBitmap(myNewImage);

        }
    }


}
