package com.example.sadokmm.appmaterial.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sadokmm.appmaterial.Adapters.PageAdapter;
import com.example.sadokmm.appmaterial.Adapters.PageAdapterFirst;
import com.example.sadokmm.appmaterial.R;
import com.example.sadokmm.appmaterial.objects.Admin;
import com.example.sadokmm.appmaterial.objects.Commentaire;
import com.example.sadokmm.appmaterial.objects.Plan;
import com.example.sadokmm.appmaterial.objects.Shop;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class FirstMain extends AppCompatActivity {


    private PageAdapterFirst pageAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    public static ArrayList<Admin> listAdmin=new ArrayList<>();
    public static ArrayList<Plan> myListPlan=new ArrayList<>();
    public static ArrayList<Shop> myListShop=new ArrayList<>();

    public static ArrayList<Plan> myListPlanFood=new ArrayList<>();
    public static ArrayList<Plan> myListPlanDrink=new ArrayList<>();

    public static final String MY_SP_FILE = "com.example.sadokmm.appmaterial.activities.users";




    LinearLayout l1,l2;
    TextView btnsub;
    Animation uptodown,downtoup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testanim);





        btnsub = (TextView) findViewById(R.id.buttonsub);
        l1 = (LinearLayout) findViewById(R.id.l1);
        l2 = (LinearLayout) findViewById(R.id.l2);
        uptodown = AnimationUtils.loadAnimation(this,R.anim.uptodown);
        downtoup = AnimationUtils.loadAnimation(this,R.anim.downtoup);
        l1.setAnimation(uptodown);
        l2.setAnimation(downtoup);

        btnsub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_first_main);
                tabLayout=(TabLayout)findViewById(R.id.tabLayoutFirst);
                viewPager=(ViewPager) findViewById(R.id.viewPagerFirst);



                tabLayout.addTab(tabLayout.newTab().setText(""));
                tabLayout.addTab(tabLayout.newTab().setText(""));

                pageAdapter=new PageAdapterFirst(getSupportFragmentManager(),tabLayout.getTabCount());
                viewPager.setAdapter(pageAdapter);
                tabLayout.setupWithViewPager(viewPager);
            }
        });


        chargerListeAdmin();
        chargerListPlan();
        chargerListShop();
        chargerListFoodOrDrink();


        SharedPreferences prefs=getSharedPreferences(MY_SP_FILE,MODE_PRIVATE);
        String user=prefs.getString("connectUser","a");

        if (!(user.equals("a"))) {

            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("admin", user);
            startActivity(intent);

        }



    }



    private void chargerListeAdmin(){


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;

        Bitmap sadok,amine,fedi,boha;


        sadok= BitmapFactory.decodeResource(getResources(),R.drawable.sadok,options);
        amine=BitmapFactory.decodeResource(getResources(),R.drawable.amine,options);
        boha=BitmapFactory.decodeResource(getResources(),R.drawable.boha,options);
        fedi=BitmapFactory.decodeResource(getResources(),R.drawable.fedi,options);






        listAdmin.add(new Admin("Sadok Mhiri","sadokmhiri@gmail.com","1223", sadok,0));
        listAdmin.add(new Admin("Amine Rassas","aminerassas@gmail.com","lol123", amine,1000));
        listAdmin.add(new Admin("Baha Bettaieb","bahabettaieb@gmail.com","1234", boha,2000));
        listAdmin.add(new Admin("Fedi Kantaoui","fedikantaoui@gmail.com","12345", fedi,3000));
        listAdmin.add(new Admin("Ahmed Ali","a","a",BitmapFactory.decodeResource(getResources(),R.drawable.av,options),4000));


        /*listAdmin.add(new Admin("Sadok Mhiri","sadokmhiri@gmail.com","1223", getResizedBitmap(sadok,450),0));
        listAdmin.add(new Admin("Amine Rassas","aminerassas@gmail.com","lol123", getResizedBitmap(amine,450),0));
        listAdmin.add(new Admin("Baha Bettaieb","bahabettaieb@gmail.com","1234", getResizedBitmap(boha,450),0));
        listAdmin.add(new Admin("Fedi Kantaoui","fedikantaoui@gmail.com","12345", getResizedBitmap(fedi,450),0));
        listAdmin.add(new Admin("Ahmed Ali","a","a",BitmapFactory.decodeResource(getResources(),R.drawable.av),2));
        */
    }



    public void chargerListPlan(){
/*
            myListPlan.add(new Plan("Pizza au feu de bois","Trés bon plan bniiina barchaaa yaatihom essaha" , (BitmapFactory.decodeResource(getResources(),R.drawable.pizza)), "BP" , 12 , 4 , "Jars AND Co","Food")) ;
            myListPlan.add(new Plan("Ma9loub","haja bnina w ndhifa" , (BitmapFactory.decodeResource(getResources(),R.drawable.ma9loub)), "BP" , 4.5 , 4.5 , "Jars AND Co","Food")) ;
            myListPlan.add(new Plan("Spaghetti","cv passable" , (BitmapFactory.decodeResource(getResources(),R.drawable.spaghetti)), "BP" , 15 , 3.5 , "Jars AND Co","Food")) ;
            myListPlan.add(new Plan("Pancake","Bniiiiin tekel wrah sweb3ek" , (BitmapFactory.decodeResource(getResources(),R.drawable.pancake)), "BP" , 6.0 , 4 , "Venti","Food")) ;
            myListPlan.add(new Plan("Crepe","hakeka w bara moush bnina barsha" , (BitmapFactory.decodeResource(getResources(),R.drawable.crepe)), "MP" , 5.0 , 2 , "Venti","Food")) ;
            myListPlan.add(new Plan("Burger","khit hacha n3imetrabi" , (BitmapFactory.decodeResource(getResources(),R.drawable.burgerr)), "MP" , 10 , 0.5 , "Jars AND Co","Food")) ;
            myListPlan.add(new Plan("Frappuccino" , "Bniiin barcha w m3ebii dorna fih 2 personnes w mekamalnehouch tellement 9wi ", BitmapFactory.decodeResource(getResources(),R.drawable.frappuccino),"BP",5.5,4.5,"The Diesel","Drink"));
            myListPlan.add(new Plan("Milkshake" , "Bniiin barcha w m3ebii dorna fih 2 personnes w mekamalnehouch tellement 9wi ", BitmapFactory.decodeResource(getResources(),R.drawable.milkshake),"BP",5.5,4.5,"The Diesel","Drink"));
            myListPlan.add(new Plan("Jwejem" , "Bniiin barcha w m3ebii dorna fih 2 personnes w mekamalnehouch tellement 9wi ", BitmapFactory.decodeResource(getResources(),R.drawable.jwejem),"BP",5.5,4.5,"L'Osmoze","Drink"));
      */




        Bitmap b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;



        b1=BitmapFactory.decodeResource(getResources(),R.drawable.pizza,options);
        b2=BitmapFactory.decodeResource(getResources(),R.drawable.ma9loub,options);
        b3=BitmapFactory.decodeResource(getResources(),R.drawable.cappuccino_2,options);
        b4=BitmapFactory.decodeResource(getResources(),R.drawable.smoothie_2,options);
        b5=BitmapFactory.decodeResource(getResources(),R.drawable.cafeglace_2,options);
        b6=BitmapFactory.decodeResource(getResources(),R.drawable.spaghetti,options);
        b7=BitmapFactory.decodeResource(getResources(),R.drawable.pancake,options);
        b8=BitmapFactory.decodeResource(getResources(),R.drawable.crepe,options);
        b9=BitmapFactory.decodeResource(getResources(),R.drawable.burgerr,options);
        b10=BitmapFactory.decodeResource(getResources(),R.drawable.frappuccino_2,options);
        b11=BitmapFactory.decodeResource(getResources(),R.drawable.milkshake_2,options);
        b12=BitmapFactory.decodeResource(getResources(),R.drawable.jjjjjjj,options);


            /*b1=getResizedBitmap(b1,500);
            b2=getResizedBitmap(b2,500);
            b3=getResizedBitmap(b3,500);
            b4=getResizedBitmap(b4,500);
            b5=getResizedBitmap(b5,500);
            b6=getResizedBitmap(b6,500);
            b7=getResizedBitmap(b7,500);
            b8=getResizedBitmap(b8,500);
            b9=getResizedBitmap(b9,500);
            b10=getResizedBitmap(b10,500);
            b11=getResizedBitmap(b11,500);
            b12=getResizedBitmap(b12,500);*/







        myListPlan.add(new Plan("Pizza au feu de bois","Trés bon plan bniiina barchaaa yaatihom essaha" ,b1 , "BP" , 12 , 4 , "Jars AND Co","Food","sadokmhiri@gmail.com")) ;
        myListPlan.add(new Plan("Ma9loub","haja bnina w ndhifa" , b2, "BP" , 4.5 , 4.5 , "Jars AND Co","Food","sadokmhiri@gmail.com")) ;

        myListPlan.add(new Plan("Cappuccino" , "Bniiin barcha w m3ebii dorna fih 2 personnes w mekamalnehouch tellement 9wi ",b3,"BP",5.5,4.5,"L'Osmoze","Drink","sadokmhiri@gmail.com"));
        myListPlan.add(new Plan("Smoothie" , "Bniiin barcha w m3ebii dorna fih 2 personnes w mekamalnehouch tellement 9wi ",b4,"MP",5.5,4.5,"The Diesel","Drink","fedikantaoui@gmail.com"));
        myListPlan.add(new Plan("Café Glacé" , "Bniiin barcha w m3ebii dorna fih 2 personnes w mekamalnehouch tellement 9wi ",b5,"BP",5.5,4.5,"L'Osmoze","Drink","aminerassas@gmail.com"));

        myListPlan.add(new Plan("Spaghetti","cv passable" , b6, "BP" , 15 , 3.5 , "Jars AND Co","Food","sadokmhiri@gmail.com")) ;
        myListPlan.add(new Plan("Pancake","Bniiiiin tekel wrah sweb3ek" , b7,"BP" , 6.0 , 4 , "Venti","Food","sadokmhiri@gmail.com")) ;
        myListPlan.add(new Plan("Crepe","hakeka w bara moush bnina barsha" ,b8, "MP" , 5.0 , 2 , "Venti","Food","sadokmhiri@gmail.com")) ;
        myListPlan.add(new Plan("Burger","khit hacha n3imetrabi" , b9, "MP" , 10 , 0.5 , "Jars AND Co","Food","sadokmhiri@gmail.com")) ;
        myListPlan.add(new Plan("Frappuccino" , "Bniiin barcha w m3ebii dorna fih 2 personnes w mekamalnehouch tellement 9wi ",b10,"BP",5.5,4.5,"The Diesel","Drink","sadokmhiri@gmail.com"));
        myListPlan.add(new Plan("Milkshake" , "Bniiin barcha w m3ebii dorna fih 2 personnes w mekamalnehouch tellement 9wi ",b11,"BP",5.5,4.5,"The Diesel","Drink","aminerassas@gmail.com"));
        myListPlan.add(new Plan("Jwejem" , "Bniiin barcha w m3ebii dorna fih 2 personnes w mekamalnehouch tellement 9wi ",b12,"BP",5.5,4.5,"L'Osmoze","Drink","aminerassas@gmail.com"));



        //like test
        myListPlan.get(0).getLikes().add("sadokmhiri@gmail.com");
        myListPlan.get(0).getLikes().add("aminerassas@gmail.com");


        //comm test

        Bitmap sadok= BitmapFactory.decodeResource(getResources(),R.drawable.sadok,options);
        Bitmap amine=BitmapFactory.decodeResource(getResources(),R.drawable.amine,options);
        Bitmap boha=BitmapFactory.decodeResource(getResources(),R.drawable.boha,options);
        Bitmap fedi=BitmapFactory.decodeResource(getResources(),R.drawable.fedi,options);
        myListPlan.get(0).getComms().add(new Commentaire("Sadok Mhiri: ","Oui je confirme ",sadok,"sadokmhiri@gmail.com"));
        myListPlan.get(0).getComms().add(new Commentaire("Amine Rassas: ","Non moush shih mshitlou barsha marrat w ma3jebnish masset lasset rodo belkom to7slouuu ",amine,"aminerassas@gmail.com"));
        myListPlan.get(0).getComms().add(new Commentaire("Fedi Kantaoui: ","Non moush shih mshitlou barsha marrat w ma3jebnish masset lasset rodo belkom to7slouuu ",fedi,"fedikantaoui@gmail.com"));

        myListPlan.get(1).getComms().add(new Commentaire("","pas de commentaires",null,"a"));

        ArrayList<Bitmap> imm=new ArrayList<>();
        imm.add(b1);
        imm.add(b2);
        imm.add(b4);
        imm.add(b5);


        myListPlan.get(0).setPhotoList(imm);
        myListPlan.get(1).setPhotoList(imm);







    }


    //List Shop

    public void chargerListShop(){

        //Diesel photo
        Bitmap diesel= BitmapFactory.decodeResource(getResources(),R.drawable.d5);
        Bitmap osmoze= BitmapFactory.decodeResource(getResources(),R.drawable.osmoze);
        Bitmap jars= BitmapFactory.decodeResource(getResources(),R.drawable.jars);
        Bitmap venti= BitmapFactory.decodeResource(getResources(),R.drawable.venti);

        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inSampleSize = 2;

        Bitmap lagustas=BitmapFactory.decodeResource(getResources(),R.drawable.lagustas,o);
        Bitmap factory=BitmapFactory.decodeResource(getResources(),R.drawable.factory,o);
        Bitmap centq= BitmapFactory.decodeResource(getResources(),R.drawable.centxqutresvingts,o);
        Bitmap thestreet=BitmapFactory.decodeResource(getResources(),R.drawable.thestreet,o);
        Bitmap highsquar=BitmapFactory.decodeResource(getResources(),R.drawable.highsquare,o);
        Bitmap bigben= BitmapFactory.decodeResource(getResources(),R.drawable.bigben,o);
        Bitmap wassila= BitmapFactory.decodeResource(getResources(),R.drawable.wassila,o);
        Bitmap coin=BitmapFactory.decodeResource(getResources(),R.drawable.coin1,o);

        //Bitmap diesel= BitmapFactory.decodeResource(getResources(),R.drawable.d5);

        myListShop.add(new Shop("The Diesel" , "25 Rue d'Alexandrie Hammam Sousse" , getResizedBitmap(diesel,500) , getListByNameShop("The Diesel") , 5.0 ));
        myListShop.add(new Shop("L'Osmoze" , "40 Rue Abdelaaziz nouira Khezama Sousse" , getResizedBitmap(osmoze,500) , getListByNameShop("L'Osmoze") , 4.0 ));
        myListShop.add(new Shop("Venti" , "36 Rue de la Poste Khezama Sousse" , getResizedBitmap(venti,500) , getListByNameShop("Venti") , 2.5 ));
        myListShop.add(new Shop("Jars AND Co" , "22 Rue de la Poste Khezama Sousse" , getResizedBitmap(jars,500) , getListByNameShop("Jars AND Co") , 1.5 ));




        myListShop.add(new Shop("Lagustas","Palais Présidentiel Skanes Monastir",lagustas,getListByNameShop("Lagustas"),4.0));
        myListShop.add(new Shop("Factory","Avenue Yasser Arafet Sahloul",factory,getListByNameShop("Factory"),3.0));
        myListShop.add(new Shop("180 Degrés","Avenue Yasser Arafet Sahloul",centq,getListByNameShop("180 Degrés"),3.0));
        myListShop.add(new Shop("The Street","Avenue Mohamed Ben Hssan Khzema East",thestreet,getListByNameShop("The Street"),2.5));
        myListShop.add(new Shop("High Square","Boulevard 14 Janvier Khezama",highsquar,getListByNameShop("High Square"),2.0));
        myListShop.add(new Shop("Big Ben","Boulevard 14 Janvier Khezama",bigben,getListByNameShop("Big Ben"),3.5));
        myListShop.add(new Shop("Wassila","Avenue les palmiers Monastir",wassila,getListByNameShop("Wassila"),3.5));
        myListShop.add(new Shop("Coin d'Alma","Boulevard 14 Janvier Khezama",coin,getListByNameShop("Coin d'Alma"),4.0));

    }



    public void chargerListFoodOrDrink(){

        for (int i=0; i<myListPlan.size() ; i++ ){

            if (myListPlan.get(i).getGenre().equals("Food")){
                myListPlanFood.add(myListPlan.get(i));
            }

        }

        for (int i=0; i<myListPlan.size() ; i++ ){

            if (myListPlan.get(i).getGenre().equals("Drink")){
                myListPlanDrink.add(myListPlan.get(i));
            }

        }


    }



    public ArrayList<Plan> getListByNameShop(String nameShop) {

        ArrayList<Plan> list = new ArrayList<>();

        for (int i = 0; i < myListPlan.size(); i++) {

            if (myListPlan.get(i).getLocalisation().equals(nameShop)) {

                list.add(myListPlan.get(i));

            }

        }
        return list;
    }



    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }
}
