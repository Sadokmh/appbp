package com.example.sadokmm.appmaterial.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sadokmm.appmaterial.R;
import com.example.sadokmm.appmaterial.activities.PlanActivity;
import com.example.sadokmm.appmaterial.objects.Plan;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class AdapterForShopGallery extends RecyclerView.Adapter<AdapterForShopGallery.ViewHolder> {


    private ArrayList<Bitmap> myList=new ArrayList<>();
    private LayoutInflater layoutInflater;


    public AdapterForShopGallery(Context context) {
        layoutInflater=LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=layoutInflater.inflate(R.layout.custom_shop_gallery_item,viewGroup,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        Bitmap current=myList.get(i);
        viewHolder.itemThumbnail.setImageBitmap(current);


        final int position=i;


    }

    @Override
    public int getItemCount() {
        return myList.size();
    }

    public void setMyList(ArrayList<Bitmap> list){
        myList=list;
        notifyItemRangeChanged(0,list.size());
    }


    //

    static class ViewHolder extends RecyclerView.ViewHolder {


        private ImageView itemThumbnail;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            itemThumbnail=(ImageView)itemView.findViewById(R.id.itemPhoto);

            // planType = (ImageView)itemView.findViewById(R.id.typePlan);

        }
    }
}
