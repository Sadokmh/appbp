package com.example.sadokmm.appmaterial.objects;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Admin {

    private String username;
    private String email;
    private String pass;
    private Bitmap photo;
    private int niveau;
    private Date registerDate;


    public Admin(String username, String email, String pass, Bitmap photo, int niveau) {
        this.username = username;
        this.email = email;
        this.pass = pass;
        this.photo = photo;
        this.niveau = niveau;
        this.registerDate= Calendar.getInstance().getTime();
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }

    public int getNiveau() {
        return niveau;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public String getStringRegisterDate() {
        DateFormat df = new SimpleDateFormat(" d MMM yyyy, HH:mm");
        String date = df.format(this.registerDate);
        return date;
    }
}
