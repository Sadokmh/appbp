package com.example.sadokmm.appmaterial.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.sadokmm.appmaterial.R;
import com.example.sadokmm.appmaterial.activities.PlanActivity;
import com.example.sadokmm.appmaterial.objects.Plan;

import java.util.ArrayList;

import static com.example.sadokmm.appmaterial.activities.FirstMain.myListPlan;


public class AdapterForPlanPhotoList extends RecyclerView.Adapter<AdapterForPlanPhotoList.ViewHolder> {


    public static ArrayList<Bitmap> planPhotoList = new ArrayList<>();
    private LayoutInflater layoutInflater;


    public AdapterForPlanPhotoList(Context context) {
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public AdapterForPlanPhotoList.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.custom_item_plan_photo, viewGroup, false);
        AdapterForPlanPhotoList.ViewHolder viewHolder = new AdapterForPlanPhotoList.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull AdapterForPlanPhotoList.ViewHolder viewHolder, int i) {

        final Bitmap image = planPhotoList.get(i);

            viewHolder.itemPhoto.setImageBitmap(planPhotoList.get(i));

            /*ImageView myImage=viewHolder.itemPhoto;
            Glide.with(layoutInflater.getContext())
                .load("https://cms-assets.tutsplus.com/uploads/users/1499/posts/28207/final_image/final-app.jpg")
                .placeholder(R.drawable.ic_person)
                .into(myImage);*/

        final int position = i;

        viewHolder.itemPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return planPhotoList.size();
    }

    public void setMyList(ArrayList<Bitmap> list) {
        planPhotoList = list;
        notifyItemRangeChanged(0, list.size());
    }



    //Afficher l'Image du plan
    public void showImage(int i) {
        final Dialog builder = new Dialog(layoutInflater.getContext());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.BLACK));


        View view = layoutInflater.inflate(R.layout.image_layout, null);

        ImageView im=(ImageView)view.findViewById(R.id.myImgView);
        ImageView close=(ImageView)view.findViewById(R.id.closeBtn);

        im.setImageBitmap(planPhotoList.get(i));

        View imageLayout=layoutInflater.inflate(R.layout.image_layout,null);

        builder.addContentView(view,new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.hide();
                builder.dismiss();
            }
        });

        builder.show();


    }



    //

    static class ViewHolder extends RecyclerView.ViewHolder  {


        private ImageView itemPhoto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            itemPhoto = (ImageView) itemView.findViewById(R.id.custom_plan_photo);


        }


    }




}

