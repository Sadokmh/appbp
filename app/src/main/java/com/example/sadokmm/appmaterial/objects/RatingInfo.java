package com.example.sadokmm.appmaterial.objects;

public class RatingInfo {

    private String ratingUser;
    private double ratingValue;


    public RatingInfo(String ratingUser, double ratingValue) {
        this.ratingUser = ratingUser;
        this.ratingValue = ratingValue;
    }


    public String getRatingUser() {
        return ratingUser;
    }

    public void setRatingUser(String ratingUser) {
        this.ratingUser = ratingUser;
    }

    public double getRatingValue() {
        return ratingValue;
    }

    public void setRatingValue(double ratingValue) {
        this.ratingValue = ratingValue;
    }
}
