package com.example.sadokmm.appmaterial.fragments;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.bumptech.glide.request.target.Target;
import com.example.sadokmm.appmaterial.Adapters.AdapterForList;
import com.example.sadokmm.appmaterial.R;
import com.example.sadokmm.appmaterial.objects.Plan;

import java.util.ArrayList;

import static com.example.sadokmm.appmaterial.activities.FirstMain.myListPlan;
import static com.example.sadokmm.appmaterial.activities.FirstMain.myListPlanFood;
import static com.example.sadokmm.appmaterial.activities.MainActivity.currentFragment;
import static com.example.sadokmm.appmaterial.activities.MainActivity.searchView;

public class FoodFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    //public static ArrayList<Plan> myList=new ArrayList<>();
    private RequestQueue requestQueue;
    //private VolleySingleton volleySingleton;
    private RecyclerView mlisteItem;
    public static AdapterForList adapterForList;




    private OnFragmentInteractionListener mListener;

    private SwipeRefreshLayout mySwipe;

    public FoodFragment() {
        // Required empty public constructor
        currentFragment = 1;
    }


    // TODO: Rename and change types and number of parameters
    public static FoodFragment newInstance(String param1, String param2) {
        FoodFragment fragment = new FoodFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =inflater.inflate(R.layout.fragment_view, container, false);

        adapterForList=new AdapterForList(getActivity()) ;
        adapterForList.setMyList(myListPlanFood);
        mlisteItem=view.findViewById(R.id.listItemHits);
        mlisteItem.setLayoutManager(new GridLayoutManager(getActivity(),2));

        mlisteItem.setAdapter(adapterForList);


        //swipe
        mySwipe=(SwipeRefreshLayout)view.findViewById(R.id.mySwipe);
        mySwipe.setOnRefreshListener(this);





        return view;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        Toast.makeText(getContext(),"Refresh successfully done",Toast.LENGTH_SHORT).show();

        adapterForList.notifyDataSetChanged();

        mySwipe.setRefreshing(false);

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
