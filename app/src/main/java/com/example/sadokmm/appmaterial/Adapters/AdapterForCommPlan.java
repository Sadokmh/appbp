package com.example.sadokmm.appmaterial.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sadokmm.appmaterial.R;
import com.example.sadokmm.appmaterial.activities.Profile;
import com.example.sadokmm.appmaterial.objects.Commentaire;
import com.example.sadokmm.appmaterial.objects.Plan;
import com.pkmmte.view.CircularImageView;

import java.util.ArrayList;

import static com.example.sadokmm.appmaterial.activities.FirstMain.listAdmin;
import static com.example.sadokmm.appmaterial.activities.MainActivity.admin;

public class AdapterForCommPlan extends RecyclerView.Adapter<AdapterForCommPlan.ViewHolder> {

    private ArrayList<Commentaire> myListComm=new ArrayList<>();
    private LayoutInflater layoutInflater;
    private int niveau;



    public AdapterForCommPlan(Context context){

            layoutInflater=LayoutInflater.from(context);

        }

    @NonNull
    @Override
    public AdapterForCommPlan.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view=layoutInflater.inflate(R.layout.comm,viewGroup,false);
        AdapterForCommPlan.ViewHolder viewHolder=new AdapterForCommPlan.ViewHolder(view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        final Commentaire commentaire=myListComm.get(i);
        viewHolder.userName.setText(commentaire.getUserName());
        viewHolder.comment.setText(commentaire.getCommentaire());
        viewHolder.userPhoto.setImageBitmap(commentaire.getUserPhoto());
        viewHolder.date.setText(commentaire.getStringDate());

      //gamification
        niveau=0;

        for (int j=0;j<listAdmin.size();j++){
            if (listAdmin.get(j).getEmail().equals(commentaire.getEmailUser())) {
                niveau=listAdmin.get(j).getNiveau();
            }

        }

        if(niveau<1000) {
            viewHolder.nbstars.setImageResource(R.drawable.nejma);
        }
        else if(niveau<2000) {
            viewHolder.nbstars.setImageResource(R.drawable.zouznjoum);
        }
        else if(niveau<3000) {
            viewHolder.nbstars.setImageResource(R.drawable.tlethanjoum);
        }
        else if(niveau<4000) {
            viewHolder.nbstars.setImageResource(R.drawable.arbaanjoum);
        }
        else {
            viewHolder.nbstars.setImageResource(R.drawable.cinqetoiles);
        }



     //Afficher / masquer le bouton supprimer
        if (commentaire.getEmailUser().equals(admin.getEmail()))
        {
            viewHolder.deleteComm.setVisibility(View.VISIBLE);
        }
        else
            {
            viewHolder.deleteComm.setVisibility(View.INVISIBLE);
        }




        viewHolder.userPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(layoutInflater.getContext(), Profile.class);
                intent.putExtra("email",commentaire.getEmailUser());
                intent.putExtra("type","profile");
                layoutInflater.getContext().startActivity(intent);
            }
        });

        final int position=i;
        viewHolder.deleteComm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myListComm.remove(position);
              //gamification
                admin.setNiveau(admin.getNiveau()-10);
                notifyDataSetChanged();
            }
        });


    }

    @Override
    public int getItemCount() {
        return myListComm.size();
    }


    public void setMyList(ArrayList<Commentaire> list){
        myListComm=list;
        notifyItemRangeChanged(0,list.size());
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView userName,comment,deleteComm,date;
        private ImageView nbstars;
        private CircularImageView userPhoto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            userName=(TextView)itemView.findViewById(R.id.usernameComm);
            comment=(TextView)itemView.findViewById(R.id.commentaireComm);
            userPhoto=(CircularImageView) itemView.findViewById(R.id.imageProfileComm);
            deleteComm=(TextView)itemView.findViewById(R.id.deleteComm);
            date=(TextView)itemView.findViewById(R.id.date);
            nbstars=(ImageView)itemView.findViewById(R.id.nbstars);

        }
    }
}
