package com.example.sadokmm.appmaterial.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.example.sadokmm.appmaterial.Adapters.AdapterForListShop;
import com.example.sadokmm.appmaterial.R;
import com.example.sadokmm.appmaterial.activities.ShopActivity;
import com.example.sadokmm.appmaterial.objects.Shop;

import java.util.ArrayList;

import static com.example.sadokmm.appmaterial.activities.FirstMain.myListShop;
import static com.example.sadokmm.appmaterial.activities.MainActivity.currentFragment;
import static com.example.sadokmm.appmaterial.activities.MainActivity.searchView;

public class LoungeFragment extends Fragment  implements SwipeRefreshLayout.OnClickListener, SwipeRefreshLayout.OnRefreshListener {




    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;


    private RecyclerView mlisteItem;
    public static AdapterForListShop adapterForListShop;




    private FoodFragment.OnFragmentInteractionListener mListener;

    private SwipeRefreshLayout mySwipe;

    public LoungeFragment() {
        // Required empty public constructor
        currentFragment = 3;
    }



        public static LoungeFragment newInstance(String param1, String param2) {
            LoungeFragment fragment = new LoungeFragment();
            Bundle args = new Bundle();
            args.putString(ARG_PARAM1, param1);
            args.putString(ARG_PARAM2, param2);
            fragment.setArguments(args);
            return fragment;
        }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view =inflater.inflate(R.layout.fragment_shop, container, false);


        adapterForListShop=new AdapterForListShop(getContext()) ;
        adapterForListShop.setMyList(myListShop);
        mlisteItem=view.findViewById(R.id.listItemHits);
        mlisteItem.setLayoutManager(new LinearLayoutManager(getContext()));

        mlisteItem.setAdapter(adapterForListShop);


        //swipe
        mySwipe=(SwipeRefreshLayout)view.findViewById(R.id.mySwipe);
        mySwipe.setOnRefreshListener(this);





        return view;

    }



    @Override
    public void onClick(View view) {

    }

    @Override
    public void onRefresh() {
        Toast.makeText(getContext(),"Refresh successfully done",Toast.LENGTH_SHORT).show();
        adapterForListShop.notifyDataSetChanged();
        mySwipe.setRefreshing(false);
    }
}
