package com.example.sadokmm.appmaterial.objects;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import android.widget.RatingBar;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Plan implements Serializable {
    private String titre;
    private String description;
    private Bitmap image;
    private String type;
    private double prix;
    private double rating ;
    private String localisation;
    private String genre;
    private String admin;
    private ArrayList<String> likes;
    private ArrayList<Commentaire> comms;
    private ArrayList<Bitmap> photoList;
    private Date date;


    public Plan(String titre, String description, Bitmap image, String type, double prix, double rating, String localisation , String genre , String admin) {
        this.titre = titre;
        this.description = description;
        this.image = image;
        this.type = type;
        this.prix = prix;
        this.rating = rating;
        this.localisation = localisation;
        this.genre = genre;
        this.admin = admin;
        likes = new ArrayList<>();
        comms = new ArrayList<>();
        photoList = new ArrayList<>();
        this.date= Calendar.getInstance().getTime();





    }






    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getLocalisation() {
        return localisation;
    }

    public void setLocalisation(String localisation) {
        this.localisation = localisation;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public ArrayList<String> getLikes() {
        return likes;
    }

    public void setLikes(ArrayList<String> likes) {
        this.likes = likes;
    }

    public ArrayList<Commentaire> getComms() {
        return comms;
    }

    public ArrayList<Bitmap> getPhotoList() {
        return photoList;
    }

    public void setPhotoList(ArrayList<Bitmap> photoList) {
        this.photoList = photoList;
    }

    public void setComms(ArrayList<Commentaire> comms) {
        this.comms = comms;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStringDate() {
        DateFormat df = new SimpleDateFormat(" d MMM yyyy, HH:mm");
        String date = df.format(this.date);
        return date;
    }
}
