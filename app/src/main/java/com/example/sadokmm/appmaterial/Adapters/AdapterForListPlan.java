package com.example.sadokmm.appmaterial.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sadokmm.appmaterial.R;
import com.example.sadokmm.appmaterial.activities.PlanActivity;
import com.example.sadokmm.appmaterial.objects.Plan;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import static com.example.sadokmm.appmaterial.activities.FirstMain.myListPlan;

public class AdapterForListPlan extends RecyclerView.Adapter<AdapterForListPlan.ViewHolder> {



    public static ArrayList<Plan> myListPlanShop=new ArrayList<>();
    private LayoutInflater layoutInflater;


    public AdapterForListPlan(Context context) {
        layoutInflater=LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public AdapterForListPlan.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=layoutInflater.inflate(R.layout.custom_itemplan_shop_list,viewGroup,false);
        AdapterForListPlan.ViewHolder viewHolder=new AdapterForListPlan.ViewHolder(view);
        return viewHolder;
    }



    @Override
    public void onBindViewHolder(@NonNull AdapterForListPlan.ViewHolder viewHolder, int i) {

        final Plan plan=myListPlanShop.get(i);
        viewHolder.itemTitle.setText(plan.getTitre());
        // viewHolder.itemDesc.setText(currentPersonne.getDescription());
        viewHolder.itemThumbnail.setImageBitmap(plan.getImage());
        viewHolder.prixType.setText(String.valueOf(plan.getPrix()) + " DT");

        if ( plan.getType().equals("MP") ) {
             viewHolder.planType.setImageResource(R.drawable.ic_mp2);
        }
        final int position=i;

        viewHolder.itemThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(layoutInflater.getContext(),PlanActivity.class);




                int position=0;

                for (int j=0 ; j<myListPlan.size() ; j++){

                    if (myListPlan.get(j)==plan)
                        position=j;

                }





                intent.putExtra("i",position);


                layoutInflater.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return myListPlanShop.size();
    }

    public void setMyList(ArrayList<Plan> list){
        myListPlanShop=list;
        notifyItemRangeChanged(0,list.size());
    }


    //

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView itemTitle;
        private TextView itemDesc;
        private ImageView itemThumbnail;
        private ImageView planType;
        private TextView prixType;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            itemTitle=(TextView)itemView.findViewById(R.id.pTitre);
            // itemDesc=(TextView)itemView.findViewById(R.id.itemDesc);
            itemThumbnail=(ImageView)itemView.findViewById(R.id.pImage);
            prixType=(TextView)itemView.findViewById(R.id.pPrix);
             planType = (ImageView)itemView.findViewById(R.id.pType);

        }
    }

}
