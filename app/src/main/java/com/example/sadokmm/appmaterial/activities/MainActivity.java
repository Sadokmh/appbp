package com.example.sadokmm.appmaterial.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sadokmm.appmaterial.Adapters.PageAdapter;
import com.example.sadokmm.appmaterial.R;
import com.example.sadokmm.appmaterial.fragments.DrinkFragment;
import com.example.sadokmm.appmaterial.fragments.FoodFragment;
import com.example.sadokmm.appmaterial.fragments.LoungeFragment;
import com.example.sadokmm.appmaterial.fragments.NavigationDrawerFragment;
import com.example.sadokmm.appmaterial.objects.Admin;
import com.example.sadokmm.appmaterial.objects.Commentaire;
import com.example.sadokmm.appmaterial.objects.Plan;
import com.example.sadokmm.appmaterial.objects.Shop;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;
import com.pkmmte.view.CircularImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;

import static com.example.sadokmm.appmaterial.activities.FirstMain.getResizedBitmap;
import static com.example.sadokmm.appmaterial.activities.FirstMain.listAdmin;
import static com.example.sadokmm.appmaterial.activities.FirstMain.myListPlan;
import static com.example.sadokmm.appmaterial.fragments.DrinkFragment.adapterForListDrink;
import static com.example.sadokmm.appmaterial.fragments.FoodFragment.adapterForList;
import static com.example.sadokmm.appmaterial.fragments.LoungeFragment.adapterForListShop;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,FoodFragment.OnFragmentInteractionListener{


    private PageAdapter pageAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar toolbar;
    public static Admin admin;
    public static int currentFragment;


    final int FRAGMENT_FOOD=0;
    final int FRAGMENT_DRINK=1;
    final int FRAGMENT_SHOP=2;

    FloatingActionButton actionButton;
    FloatingActionMenu actionMenu;


    //Tags for FAB menu Click Listenner
    private static final String TAG_IMAGE="imageClick";
    private static final String TAG_Video="videoClick";




    public static Bitmap bp;
    public static Bitmap mp;


    public static android.support.v7.widget.SearchView searchView;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("BON PLAN");

        searchView=new android.support.v7.widget.SearchView(this);


        NavigationDrawerFragment drawerFragment=(NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.settUp(R.id.fragment_navigation_drawer,(DrawerLayout)findViewById(R.id.drawerLayout),toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout =(TabLayout) findViewById(R.id.materialTabHost);

        tabLayout.addTab(tabLayout.newTab().setText(""));
        tabLayout.addTab(tabLayout.newTab().setText(""));
        tabLayout.addTab(tabLayout.newTab().setText(""));


        pageAdapter=new PageAdapter(getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(pageAdapter);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.icon_burger);
        tabLayout.getTabAt(1).setIcon(R.drawable.drink);
        tabLayout.getTabAt(2).setIcon(R.drawable.shop);

        bp=BitmapFactory.decodeResource(getResources(),R.drawable.ic_bp2);
        mp=BitmapFactory.decodeResource(getResources(),R.drawable.ic_mp2);


        Intent intent=getIntent();

        String emailadmin=intent.getExtras().getString("admin");

        for(int i=0;i<listAdmin.size();i++){
            if (listAdmin.get(i).getEmail().equals(emailadmin)){
                this.admin=listAdmin.get(i);
            }
        }






        //SETUP the Foalting Action Button
        setUpFab();





    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.my_menu,menu);

        MenuItem search=menu.findItem(R.id.search_action);
        searchView=(android.support.v7.widget.SearchView)search.getActionView();
        searchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                currentFragment=viewPager.getCurrentItem();



                   if (currentFragment == FRAGMENT_FOOD) {
                        adapterForList.getFilter().filter(s);
                    }

                    else if (currentFragment == FRAGMENT_DRINK) {
                        adapterForListDrink.getFilter().filter(s);
                    }


                    else  {
                        adapterForListShop.getFilter().filter(s);
                    }


                return false;
            }
        });

        return true;
    }

    //SETUP the FAB
    private void setUpFab(){


        //SET UP the Floating Action Button
        // in Activity Context
        ImageView icon = new ImageView(this); // Create an icon
        icon.setImageResource(R.drawable.ic_add_black_24dp);
        actionButton = new FloatingActionButton.Builder(this)
                .setContentView(icon)
                .setBackgroundDrawable(R.drawable.selector_button_pressed)
                .build();

        SubActionButton.Builder itemBuilder = new SubActionButton.Builder(this);
        //menu items
        ImageView itemImage = new ImageView(this);
        itemImage.setImageResource(R.drawable.ic_bp2);
        SubActionButton button1 = itemBuilder.setContentView(itemImage).build();
        //item 2
        final ImageView itemVideo = new ImageView(this);
        itemVideo.setImageResource(R.drawable.ic_mp2);
        SubActionButton button2 = itemBuilder.setContentView(itemVideo).build();



        //Create the menu with the items:
        actionMenu = new FloatingActionMenu.Builder(this)
                .addSubActionView(button1)
                .addSubActionView(button2)
                // ...
                .attachTo(actionButton)
                .build();

        //set the TAGs
        button1.setTag(TAG_IMAGE);
        button2.setTag(TAG_Video);

        //Create Click listenners
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);

    }





    //Hiding the FAB when DL is open
    private void toggleTranslateFAB(float slideOffset){

        if (actionMenu != null) {
            if (actionMenu.isOpen()){
                actionMenu.close(true);
            }
            actionButton.setTranslationX(slideOffset * 200);
        }

    }


    public void onDrawerSlide(float slideOffset){

        toggleTranslateFAB(slideOffset);

    }

    @Override
    public void onClick(View view) {

        if (view.getTag().equals(TAG_IMAGE)) {
            Toast.makeText(getApplicationContext(),"BP",Toast.LENGTH_SHORT).show();
            ajouterPlan("BP");

        }

        if (view.getTag().equals(TAG_Video)) {
            Toast.makeText(getApplicationContext(),"MP",Toast.LENGTH_SHORT).show();
            ajouterPlan("MP");
        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }






    //SETUP Lists

        //ListPlan

/*
        public void chargerListPlan(){

            myListPlan.add(new Plan("Pizza au feu de bois","Trés bon plan bniiina barchaaa yaatihom essaha" , (BitmapFactory.decodeResource(getResources(),R.drawable.pizza)), "BP" , 12 , 4 , "Jars AND Co","Food")) ;
            myListPlan.add(new Plan("Ma9loub","haja bnina w ndhifa" , (BitmapFactory.decodeResource(getResources(),R.drawable.ma9loub)), "BP" , 4.5 , 4.5 , "Jars AND Co","Food")) ;
            myListPlan.add(new Plan("Spaghetti","cv passable" , (BitmapFactory.decodeResource(getResources(),R.drawable.spaghetti)), "BP" , 15 , 3.5 , "Jars AND Co","Food")) ;
            myListPlan.add(new Plan("Pancake","Bniiiiin tekel wrah sweb3ek" , (BitmapFactory.decodeResource(getResources(),R.drawable.pancake)), "BP" , 6.0 , 4 , "Venti","Food")) ;
            myListPlan.add(new Plan("Crepe","hakeka w bara moush bnina barsha" , (BitmapFactory.decodeResource(getResources(),R.drawable.crepe)), "MP" , 5.0 , 2 , "Venti","Food")) ;
            myListPlan.add(new Plan("Burger","khit hacha n3imetrabi" , (BitmapFactory.decodeResource(getResources(),R.drawable.burgerr)), "MP" , 10 , 0.5 , "Jars AND Co","Food")) ;
            myListPlan.add(new Plan("Frappuccino" , "Bniiin barcha w m3ebii dorna fih 2 personnes w mekamalnehouch tellement 9wi ", BitmapFactory.decodeResource(getResources(),R.drawable.frappuccino),"BP",5.5,4.5,"The Diesel","Drink"));
            myListPlan.add(new Plan("Milkshake" , "Bniiin barcha w m3ebii dorna fih 2 personnes w mekamalnehouch tellement 9wi ", BitmapFactory.decodeResource(getResources(),R.drawable.milkshake),"BP",5.5,4.5,"The Diesel","Drink"));
            myListPlan.add(new Plan("Jwejem" , "Bniiin barcha w m3ebii dorna fih 2 personnes w mekamalnehouch tellement 9wi ", BitmapFactory.decodeResource(getResources(),R.drawable.jwejem),"BP",5.5,4.5,"L'Osmoze","Drink"));





            Bitmap b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;



            b1=BitmapFactory.decodeResource(getResources(),R.drawable.pizza,options);
            b2=BitmapFactory.decodeResource(getResources(),R.drawable.ma9loub,options);
            b3=BitmapFactory.decodeResource(getResources(),R.drawable.cappuccino_2,options);
            b4=BitmapFactory.decodeResource(getResources(),R.drawable.smoothie_2,options);
            b5=BitmapFactory.decodeResource(getResources(),R.drawable.cafeglace_2,options);
            b6=BitmapFactory.decodeResource(getResources(),R.drawable.spaghetti,options);
            b7=BitmapFactory.decodeResource(getResources(),R.drawable.pancake,options);
            b8=BitmapFactory.decodeResource(getResources(),R.drawable.crepe,options);
            b9=BitmapFactory.decodeResource(getResources(),R.drawable.burgerr,options);
            b10=BitmapFactory.decodeResource(getResources(),R.drawable.frappuccino_2,options);
            b11=BitmapFactory.decodeResource(getResources(),R.drawable.milkshake_2,options);
            b12=BitmapFactory.decodeResource(getResources(),R.drawable.jjjjjjj,options);


            b1=getResizedBitmap(b1,500);
            b2=getResizedBitmap(b2,500);
            b3=getResizedBitmap(b3,500);
            b4=getResizedBitmap(b4,500);
            b5=getResizedBitmap(b5,500);
            b6=getResizedBitmap(b6,500);
            b7=getResizedBitmap(b7,500);
            b8=getResizedBitmap(b8,500);
            b9=getResizedBitmap(b9,500);
            b10=getResizedBitmap(b10,500);
            b11=getResizedBitmap(b11,500);
            b12=getResizedBitmap(b12,500);







            myListPlan.add(new Plan("Pizza au feu de bois","Trés bon plan bniiina barchaaa yaatihom essaha" ,b1 , "BP" , 12 , 4 , "Jars AND Co","Food","sadokmhiri@gmail.com")) ;
            myListPlan.add(new Plan("Ma9loub","haja bnina w ndhifa" , b2, "BP" , 4.5 , 4.5 , "Jars AND Co","Food","sadokmhiri@gmail.com")) ;

            myListPlan.add(new Plan("Cappuccino" , "Bniiin barcha w m3ebii dorna fih 2 personnes w mekamalnehouch tellement 9wi ",b3,"BP",5.5,4.5,"L'Osmoze","Drink","sadokmhiri@gmail.com"));
            myListPlan.add(new Plan("Smoothie" , "Bniiin barcha w m3ebii dorna fih 2 personnes w mekamalnehouch tellement 9wi ",b4,"MP",5.5,4.5,"The Diesel","Drink","fedikantaoui@gmail.com"));
            myListPlan.add(new Plan("Café Glacé" , "Bniiin barcha w m3ebii dorna fih 2 personnes w mekamalnehouch tellement 9wi ",b5,"BP",5.5,4.5,"L'Osmoze","Drink","aminerassas@gmail.com"));

            myListPlan.add(new Plan("Spaghetti","cv passable" , b6, "BP" , 15 , 3.5 , "Jars AND Co","Food","sadokmhiri@gmail.com")) ;
            myListPlan.add(new Plan("Pancake","Bniiiiin tekel wrah sweb3ek" , b7,"BP" , 6.0 , 4 , "Venti","Food","sadokmhiri@gmail.com")) ;
            myListPlan.add(new Plan("Crepe","hakeka w bara moush bnina barsha" ,b8, "MP" , 5.0 , 2 , "Venti","Food","sadokmhiri@gmail.com")) ;
            myListPlan.add(new Plan("Burger","khit hacha n3imetrabi" , b9, "MP" , 10 , 0.5 , "Jars AND Co","Food","sadokmhiri@gmail.com")) ;
            myListPlan.add(new Plan("Frappuccino" , "Bniiin barcha w m3ebii dorna fih 2 personnes w mekamalnehouch tellement 9wi ",b10,"BP",5.5,4.5,"The Diesel","Drink","sadokmhiri@gmail.com"));
            myListPlan.add(new Plan("Milkshake" , "Bniiin barcha w m3ebii dorna fih 2 personnes w mekamalnehouch tellement 9wi ",b11,"BP",5.5,4.5,"The Diesel","Drink","aminerassas@gmail.com"));
            myListPlan.add(new Plan("Jwejem" , "Bniiin barcha w m3ebii dorna fih 2 personnes w mekamalnehouch tellement 9wi ",b12,"BP",5.5,4.5,"L'Osmoze","Drink","aminerassas@gmail.com"));



            //like test
            myListPlan.get(0).getLikes().add("sadokmhiri@gmail.com");
            myListPlan.get(0).getLikes().add("aminerassas@gmail.com");


            //comm test

            Bitmap sadok= BitmapFactory.decodeResource(getResources(),R.drawable.sadok,options);
            Bitmap amine=BitmapFactory.decodeResource(getResources(),R.drawable.amine,options);
            Bitmap boha=BitmapFactory.decodeResource(getResources(),R.drawable.boha,options);
            Bitmap fedi=BitmapFactory.decodeResource(getResources(),R.drawable.fedi,options);
            myListPlan.get(0).getComms().add(new Commentaire("Sadok Mhiri: ","Oui je confirme ",sadok,"sadokmhiri@gmail.com"));
            myListPlan.get(0).getComms().add(new Commentaire("Amine Rassas: ","Non moush shih mshitlou barsha marrat w ma3jebnish masset lasset rodo belkom to7slouuu ",amine,"aminerassas@gmail.com"));
            myListPlan.get(0).getComms().add(new Commentaire("Fedi Kantaoui: ","Non moush shih mshitlou barsha marrat w ma3jebnish masset lasset rodo belkom to7slouuu ",fedi,"fedikantaoui@gmail.com"));

        }


        //List Shop

        public void chargerListShop(){

            //Diesel photo
            Bitmap diesel= BitmapFactory.decodeResource(getResources(),R.drawable.d5);
            Bitmap osmoze= BitmapFactory.decodeResource(getResources(),R.drawable.osmoze);
            Bitmap jars= BitmapFactory.decodeResource(getResources(),R.drawable.jars);
            Bitmap venti= BitmapFactory.decodeResource(getResources(),R.drawable.venti);
            //Bitmap diesel= BitmapFactory.decodeResource(getResources(),R.drawable.d5);

            myListShop.add(new Shop("The Diesel" , "25 Rue d'Alexandrie Hammam Sousse" , getResizedBitmap(diesel,500) , getListByNameShop("The Diesel") , 5.0 ));
            myListShop.add(new Shop("L'Osmoze" , "40 Rue Abdelaaziz nouira Khezama Sousse" , getResizedBitmap(osmoze,500) , getListByNameShop("L'Osmoze") , 4.0 ));
            myListShop.add(new Shop("Venti" , "36 Rue de la Poste Khezama Sousse" , getResizedBitmap(venti,500) , getListByNameShop("Venti") , 2.5 ));
            myListShop.add(new Shop("Jars AND Co" , "22 Rue de la Poste Khezama Sousse" , getResizedBitmap(jars,500) , getListByNameShop("Jars AND Co") , 1.5 ));


        }

*/





        public void ajouterPlan(String type){

            Intent intent=new Intent(this,AjouterPlan.class);

            intent.putExtra("type",type);

            startActivity(intent);

        }









    //works with the addTouchListener of a recyclerView in NavigationDrawerFragment
    public void OnDrawerItemClicked(int index){

        viewPager.setCurrentItem(index);


    }











}
