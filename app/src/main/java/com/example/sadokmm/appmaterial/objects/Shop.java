package com.example.sadokmm.appmaterial.objects;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Shop implements Serializable {


    private String nom;
    private String adresse;
    private Bitmap photo;
    private ArrayList<Plan> listPlan;
    private ArrayList<RatingInfo> ratingList;
    private double rating;


    public Shop(String nom, String adresse, Bitmap photo, ArrayList<Plan> listPlan, double rating) {
        this.nom = nom;
        this.adresse = adresse;
        this.photo = photo;
        this.listPlan = listPlan;
        this.rating = rating;
        ratingList=new ArrayList<>();
    }

/*
    protected Shop(Parcel in) {
        nom = in.readString();
        adresse = in.readString();
        photo = in.readParcelable(Bitmap.class.getClassLoader());
        listPlan = in.createTypedArrayList(Plan.CREATOR);
        rating = in.readDouble();
    }

    public static final Creator<Shop> CREATOR = new Creator<Shop>() {
        @Override
        public Shop createFromParcel(Parcel in) {
            return new Shop(in);
        }

        @Override
        public Shop[] newArray(int size) {
            return new Shop[size];
        }
    };*/

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }

    public ArrayList<Plan> getListPlan() {
        return listPlan;
    }

    public void setListPlan(ArrayList<Plan> listPlan) {
        this.listPlan = listPlan;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public ArrayList<RatingInfo> getRatingList() {
        return ratingList;
    }

    public void setRatingList(ArrayList<RatingInfo> ratingList) {
        this.ratingList = ratingList;
    }
}


