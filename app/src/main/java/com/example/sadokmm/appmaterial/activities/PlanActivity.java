package com.example.sadokmm.appmaterial.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.sadokmm.appmaterial.Adapters.AdapterForCommPlan;
import com.example.sadokmm.appmaterial.Adapters.AdapterForListPlanAdmin;
import com.example.sadokmm.appmaterial.Adapters.AdapterForPlanPhotoList;
import com.example.sadokmm.appmaterial.R;
import com.example.sadokmm.appmaterial.objects.Admin;
import com.example.sadokmm.appmaterial.objects.Commentaire;
import com.example.sadokmm.appmaterial.objects.Plan;
import com.example.sadokmm.appmaterial.objects.Shop;
import com.pkmmte.view.CircularImageView;

import static com.example.sadokmm.appmaterial.activities.FirstMain.listAdmin;
import static com.example.sadokmm.appmaterial.activities.MainActivity.admin;
import static com.example.sadokmm.appmaterial.activities.FirstMain.myListPlan;
import static com.example.sadokmm.appmaterial.activities.FirstMain.myListShop;


public class PlanActivity extends AppCompatActivity {


    private ImageView planImage;
    private TextView planTitre;
    private TextView planDescription;
    private ImageView planTypeIcon,like_icon,nbStars;
    private TextView planTypeText;
    private RatingBar planRating;
    private TextView planPrix;
    private TextView planLoc,adminNamePlan,like,nbLike,date;
    private CircularImageView imageProfile;
    private Admin profile;
    private Boolean likeBoolean;
    private TextView commenter;
    private EditText input_commentaire;
    private View viewRecycler;
    private int niveau;
    private int position;

    private Toolbar toolbar;

    private RecyclerView mlisteItem,photoList;

    private AdapterForCommPlan adap;
    private AdapterForPlanPhotoList photoListAdapter;


    private boolean isImageFitToScreen = false;


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        planImage = (ImageView) findViewById(R.id.planImage);
        planTypeIcon = (ImageView) findViewById(R.id.planTypeIcon);
        planTitre = (TextView) findViewById(R.id.planT);
        planDescription = (TextView) findViewById(R.id.planDesc);
        planPrix = (TextView) findViewById(R.id.planPrix);
        planRating = (RatingBar) findViewById(R.id.planRating);
        planTypeText = (TextView) findViewById(R.id.planTypeText);
        planLoc = (TextView) findViewById(R.id.planLocal);
        imageProfile = (CircularImageView) findViewById(R.id.image_profile);
        adminNamePlan=(TextView)findViewById(R.id.adminNamePlan);
        like = (TextView) findViewById(R.id.text_like);
        nbLike = (TextView) findViewById(R.id.nb_like);
        like_icon=(ImageView)findViewById(R.id.icon_like);
        commenter=(TextView)findViewById(R.id.commenter);
        input_commentaire=(EditText)findViewById(R.id.input_commentaire);
        date = (TextView)findViewById(R.id.date);
        nbStars=(ImageView)findViewById(R.id.nbStarsPlanAct);


        likeBoolean=false;




        final Intent intent = getIntent();
        position=intent.getExtras().getInt("i");

        final Plan plan = myListPlan.get(position);

        planTitre.setText(plan.getTitre());
        planDescription.setText(plan.getDescription());
        planPrix.setText(plan.getPrix() + " DT");
        float rate = (float) plan.getRating();
        planRating.setRating(rate);

        getSupportActionBar().setTitle(plan.getTitre() + " chez " + plan.getLocalisation());





        if (plan.getType().equals("MP")) {
            planTypeIcon.setImageResource(R.drawable.ic_mp2);
            planTypeText.setText("Mauvais Plan");

            planTypeText.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        planImage.setImageBitmap(plan.getImage());
        planLoc.setText(plan.getLocalisation());
        date.setText(plan.getStringDate());





    //adapter install
        photoListAdapter=new AdapterForPlanPhotoList(this);
        photoListAdapter.setMyList(plan.getPhotoList());
        photoListAdapter.setHasStableIds(true);

        adap=new AdapterForCommPlan(this);
        adap.setMyList(plan.getComms());
        adap.setHasStableIds(true);

    //recycler view install
        mlisteItem=(RecyclerView) findViewById(R.id.commList);
        //photoList=(RecyclerView) findViewById(R.id.recyclerViewPhoto);



        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_images);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(photoListAdapter);


        if (plan.getPhotoList().size()==0){
            viewRecycler=(View)findViewById(R.id.viewRecycler);
            viewRecycler.setVisibility(View.INVISIBLE);
            recyclerView.setVisibility(View.INVISIBLE);

        }



        /*LinearLayoutManager layoutManager= new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        photoList.setLayoutManager(new GridLayoutManager(this,4));*/
        mlisteItem.setLayoutManager(new LinearLayoutManager(this));


        //photoList.setAdapter(photoListAdapter);
        mlisteItem.setAdapter(adap);








    //chercher est-ce-que l'utilisateur connecté a aimé déja ce plan ou non

        for(int i=0;i<plan.getLikes().size();i++)
        {
            if (plan.getLikes().get(i).equals(admin.getEmail()))
            {
                likeBoolean=true;
                break;
            }
        }



        nbLike.setText(String.valueOf(plan.getLikes().size()));
        if (likeBoolean)
        {
            like_icon.setImageResource(R.drawable.ic_favorite);
            like.setText("Je n'aime plus  ");

        }



        for (int i=0;i<listAdmin.size();i++){
            if (listAdmin.get(i).getEmail().equals(plan.getAdmin())){
                profile=listAdmin.get(i);
                imageProfile.setImageBitmap(profile.getPhoto());
                adminNamePlan.setText(profile.getUsername());
                niveau=listAdmin.get(i).getNiveau();

            }
        }




      //gamification
        if(niveau<1000) {
            nbStars.setImageResource(R.drawable.nejma);
        }
        else if(niveau<2000) {
            nbStars.setImageResource(R.drawable.zouznjoum);
        }
        else if(niveau<3000) {
            nbStars.setImageResource(R.drawable.tlethanjoum);
        }
        else if(niveau<4000) {
            nbStars.setImageResource(R.drawable.arbaanjoum);
        }
        else {
            nbStars.setImageResource(R.drawable.cinqetoiles);
        }



        planImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage(position);
            }
        });



        planLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                afficherPlan(plan.getLocalisation());
            }
        });


        imageProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1=new Intent(getApplicationContext(),Profile.class);
                intent1.putExtra("email",profile.getEmail());
                intent1.putExtra("type","profile");
                startActivity(intent1);
            }
        });


        like_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!likeBoolean)
                {
                    plan.getLikes().add(admin.getEmail());
                  //gamification
                    admin.setNiveau(admin.getNiveau()+15);
                    like_icon.setImageResource(R.drawable.ic_favorite);
                    like.setText("Je n'aime plus  ");
                    likeBoolean = true;
                    nbLike.setText(String.valueOf(plan.getLikes().size()));
                }

                else
                {
                    like_icon.setImageResource(R.drawable.ic_favorite_border);
                    like.setText("J'aime  ");
                    for(int i=0;i<plan.getLikes().size();i++)
                    {
                        if (plan.getLikes().get(i).equals(admin.getEmail())){
                            plan.getLikes().remove(i);
                            break;
                        }
                    }
                    nbLike.setText(String.valueOf(plan.getLikes().size()));
                  //gamification
                    admin.setNiveau(admin.getNiveau()-15);
                    likeBoolean=false;
                }
            }
        });



        commenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String inputComm=input_commentaire.getText().toString();
                if (inputComm.isEmpty())
                {

                }
                else
                {
                    plan.getComms().add(new Commentaire(admin.getUsername(),inputComm,admin.getPhoto(),admin.getEmail()));
                    input_commentaire.getText().clear();
                    adap.notifyDataSetChanged();
                    //gamification
                      int pts= 40;
                      for(int i=0;i<plan.getComms().size();i++){
                          if (plan.getComms().get(i).getEmailUser().equals(admin.getEmail())){
                              pts = 10;
                              break;
                          }
                      }
                      admin.setNiveau(admin.getNiveau()+pts);
                }
            }
        });


    }





   //Afficher l'Image du plan
    public void showImage(int i) {
        final Dialog builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.BLACK));

        View view = getLayoutInflater().inflate(R.layout.image_layout, null);

        ImageView im=(ImageView)view.findViewById(R.id.myImgView);
        ImageView close=(ImageView)view.findViewById(R.id.closeBtn);

        im.setImageBitmap(myListPlan.get(i).getImage());

        View imageLayout=getLayoutInflater().inflate(R.layout.image_layout,null);

        builder.addContentView(view,new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.hide();
                builder.dismiss();
            }
        });

        builder.show();


    }




    public void afficherPlan(String shop){


        for ( int i=0; i<myListShop.size() ; i++ ){

            if (myListShop.get(i).getNom().equals(shop)) {

                Intent intent = new Intent(this,ShopActivity.class);


                intent.putExtra("i",myListShop.get(i).getNom());

                startActivity(intent);

            }

        }

    }

}
