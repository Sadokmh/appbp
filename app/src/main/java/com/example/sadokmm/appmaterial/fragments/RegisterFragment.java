package com.example.sadokmm.appmaterial.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sadokmm.appmaterial.R;
import com.example.sadokmm.appmaterial.activities.MainActivity;
import com.example.sadokmm.appmaterial.objects.Admin;
import com.pkmmte.view.CircularImageView;

import java.io.IOException;

import static android.app.Activity.RESULT_OK;
import static com.example.sadokmm.appmaterial.activities.FirstMain.MY_SP_FILE;
import static com.example.sadokmm.appmaterial.activities.FirstMain.listAdmin;

public class RegisterFragment extends Fragment {

    final static int OPENCAM_CODE = 22;
    final static int OPENGALLERY_CODE = 23;


    private TextView registerBtn,errorTextUsername,errorTextEmail,errorTextPass,errorTextPassConfirm,errorTextPhoto;
    private EditText email, pass, confirmPass,usernameRegister;
    private Drawable myNewImage;
    private CircularImageView photo;
    private boolean ok;


    public RegisterFragment() {


    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_register, container, false);

        usernameRegister=(EditText)view.findViewById(R.id.usernameRegister);
        registerBtn = (TextView) view.findViewById(R.id.registerBtn);
        email = (EditText) view.findViewById(R.id.emailRegister);
        pass = (EditText) view.findViewById(R.id.passRegister);
        confirmPass = (EditText) view.findViewById(R.id.confirmPassRegister);
        photo = (CircularImageView) view.findViewById(R.id.photoRegister);
        errorTextUsername = (TextView)view.findViewById(R.id.errorTextUsername);
        errorTextEmail = (TextView)view.findViewById(R.id.errorTextEmail);
        errorTextPass = (TextView)view.findViewById(R.id.errorTextPass);
        errorTextPassConfirm = (TextView)view.findViewById(R.id.errorTextPassConfirm);
        errorTextPhoto = (TextView)view.findViewById(R.id.errorTextPhoto);
        myNewImage=null;


        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ok=true;
                String usernametxt,passtxt,confrimPasstxt,emailtxt;
                        usernametxt=usernameRegister.getText().toString();
                        passtxt=pass.getText().toString();
                        confrimPasstxt=confirmPass.getText().toString();
                        emailtxt=email.getText().toString();

                        errorTextPassConfirm.setText("");
                        errorTextPass.setText("");
                        errorTextEmail.setText("");
                        errorTextUsername.setText("");
                        errorTextPhoto.setText("");

                        if (usernametxt.isEmpty())
                        {
                            errorTextUsername.setText("Veuillez saisir votre username");
                            ok=false;
                        }
                        if (!isAlpha(usernametxt))
                        {
                            errorTextUsername.setText("Username invalide");
                        }
                        if (emailtxt.isEmpty())
                        {
                            errorTextEmail.setText("Veuillez saisir votre email");
                            ok=false;
                        }
                        if (!verifEmail(emailtxt))
                        {
                            errorTextEmail.setText("Email invalide");
                            ok=false;
                        }
                        if (passtxt.isEmpty())
                         {
                             errorTextPass.setText("Veuillez saisir votre mot de passe");
                             ok=false;
                         }
                        if (confrimPasstxt.isEmpty())
                         {
                             errorTextPassConfirm.setText("Veuillez confirmer votre mot de passe");
                             ok=false;
                         }
                        if (myNewImage == null)
                         {
                             errorTextPhoto.setText("Veuillez choisir votre photo");
                             ok=false;
                         }
                         if (!(passtxt.equals(confrimPasstxt)))
                         {
                             errorTextPassConfirm.setText("Veuillez saisir le même mot de passe");
                             ok=false;
                         }


                         if (ok){

                            listAdmin.add(new Admin(usernametxt,emailtxt,passtxt,dToBitmap(myNewImage),0));

                             //Shared Preferences to save connectUser
                             SharedPreferences.Editor editor=getActivity().getSharedPreferences(MY_SP_FILE, Context.MODE_PRIVATE).edit();
                             editor.putString("connectUser",emailtxt);
                             editor.commit();
                            
                             Intent intent = new Intent(getContext(), MainActivity.class);
                             intent.putExtra("admin", emailtxt);
                             startActivity(intent);

                         }




            }
        });


        return view;


    }


    private void selectImage() {

        final String[] items = {"Camera", "Gallerie"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Choisir le source: ");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                //boolean result=Utility.checkPermission(getContext());
                if (items[item].equals("Camera")) {
                    openCam();
                } else {
                    openGallery();

                }
            }
        });

        builder.show();
    }


    private void openCam() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, OPENCAM_CODE);

        }
    }


    private void openGallery() {

        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, OPENGALLERY_CODE);//one can be replaced with any action code
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case OPENCAM_CODE:
                if (resultCode == RESULT_OK) {
                    Bundle extras = data.getExtras();
                    myNewImage = new BitmapDrawable(getResources(),(Bitmap)extras.get("data"));
                    photo.setImageDrawable(myNewImage);
                }

                break;
            case OPENGALLERY_CODE:
                if (resultCode == RESULT_OK) {
                    Uri dat= data.getData();

                    try {
                        myNewImage =  new BitmapDrawable(getResources(),MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), dat));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    photo.setImageDrawable(myNewImage);
                }
                break;
        }


    }



    boolean verifEmail(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean isAlpha(String name) {
        char[] chars = name.toCharArray();

        for (char c : chars) {
            if(!Character.isLetter(c)) {
                if (!Character.isSpaceChar(c)) return false;

            }
        }

        return true;
    }




    //convert drawable to bitmap

    public static Bitmap dToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

}
