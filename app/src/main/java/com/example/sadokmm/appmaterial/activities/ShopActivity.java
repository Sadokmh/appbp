package com.example.sadokmm.appmaterial.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sadokmm.appmaterial.Adapters.AdapterForListPlan;
import com.example.sadokmm.appmaterial.Adapters.AdapterForShopGallery;
import com.example.sadokmm.appmaterial.Adapters.AdapterImageGallery;
import com.example.sadokmm.appmaterial.R;
import com.example.sadokmm.appmaterial.objects.Plan;
import com.example.sadokmm.appmaterial.objects.RatingInfo;
import com.example.sadokmm.appmaterial.objects.Shop;

import java.util.ArrayList;

import static com.example.sadokmm.appmaterial.activities.FirstMain.myListPlan;
import static com.example.sadokmm.appmaterial.activities.FirstMain.myListShop;
import static com.example.sadokmm.appmaterial.activities.MainActivity.admin;


public class ShopActivity extends AppCompatActivity {



    private AdapterImageGallery adapter;

    private ArrayList<Bitmap> myListPhoto=new ArrayList<>();
    private ArrayList<Plan> myList=new ArrayList<>();

    private RecyclerView mlisteItem;
    private AdapterForShopGallery adapterForShopGallery;

    private AdapterForListPlan adap;
    private Toolbar toolbar;


    private TextView shopTitle,shopAdress,ratingNb;
    private ImageView shopPhoto;
    private RatingBar shopRating;
    private Boolean ratingBoolean;
    private int ratingListPosition;
    private float totalRating;
    private Shop shop;
    private RatingInfo ratingInfo;


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);







        shopAdress=(TextView)findViewById(R.id.shopAdress);
        shopTitle=(TextView)findViewById(R.id.planT);
        shopPhoto=(ImageView)findViewById(R.id.shopPhoto);
        shopRating=(RatingBar)findViewById(R.id.shopRating);
        ratingNb=(TextView)findViewById(R.id.ratingNb);

        Intent intent=getIntent();
        final String shopName=intent.getExtras().getString("i");
         int position=0;




        for (int i=0;i<myListShop.size();i++){
            if (myListShop.get(i).getNom().equals(shopName)){
                shop =myListShop.get(i);
                position=i;
                break;
            }
        }
        final int imageShopPosition;
        imageShopPosition=position;

        totalRating=calculRating(shop);
        ratingNb.setText(String.valueOf(shop.getRatingList().size()));


        getSupportActionBar().setTitle(shop.getNom());

        shopTitle.setText(shop.getNom());
        shopAdress.setText(shop.getAdresse());
        shopPhoto.setImageBitmap(shop.getPhoto());
        shopRating.setRating(totalRating);

        shopPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage(imageShopPosition);
            }
        });





        adap=new AdapterForListPlan(this);
        adap.setMyList(shop.getListPlan());
        adap.setHasStableIds(true);





        mlisteItem=(RecyclerView) findViewById(R.id.photosList);
        GridLayoutManager layoutManager = new GridLayoutManager(this,2);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mlisteItem.setLayoutManager(layoutManager);




        mlisteItem.setAdapter(adap);
        //mlisteItem.scrollToPosition(1);







         //



        shopRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {

                chercher(shop);

                if (!ratingBoolean) {
                    ratingInfo=new RatingInfo(admin.getEmail(),v);
                    shop.getRatingList().add(ratingInfo);
                    ratingBoolean=true;
                    ratingListPosition=shop.getRatingList().size()-1;
                    totalRating=calculRating(shop);
                    ratingBar.setRating(totalRating);
                    ratingNb.setText(String.valueOf(shop.getRatingList().size()));
                }
                else {
                    shop.getRatingList().remove(ratingInfo);
                    //shop.getRatingList().add(new RatingInfo(admin.getEmail(),v));
                    totalRating=calculRating(shop);
                    ratingBar.setRating(totalRating);
                    ratingNb.setText(String.valueOf(shop.getRatingList().size()));
                    ratingBoolean=false;

                }




            }
        });






    }


    private void chercher(Shop shop){
        //chercher est-ce-que l'utilisateur connecté a noté déja ce shop ou non
        ratingBoolean=false;
        for(int i=0;i<shop.getRatingList().size();i++){
            if (shop.getRatingList().get(i).getRatingUser().equals(admin.getEmail())){
                ratingInfo=shop.getRatingList().get(i);
                ratingBoolean = true;
                ratingListPosition=i;
                break;
            }
        }
    }


    private float calculRating(Shop shop){

        float ratingTotal=0;

        if (shop.getRatingList().size()!=0) {
            for (int i = 0; i < shop.getRatingList().size(); i++) {

                ratingTotal += shop.getRatingList().get(i).getRatingValue();

            }
            ratingTotal = (ratingTotal / shop.getRatingList().size());
            return ratingTotal;
        }
        else return 0;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();

        if (id == R.id.home){
            NavUtils.navigateUpFromSameTask(this);
            Toast.makeText(this,"HELLO",Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }





    //Afficher l'Image du Shop
    public void showImage(int i) {
        final Dialog builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        View view = getLayoutInflater().inflate(R.layout.image_layout, null);

        ImageView im=(ImageView)view.findViewById(R.id.myImgView);
        ImageView close=(ImageView)view.findViewById(R.id.closeBtn);

        im.setImageBitmap(myListShop.get(i).getPhoto());

        View imageLayout=getLayoutInflater().inflate(R.layout.image_layout,null);

        builder.addContentView(view,new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.hide();
                builder.dismiss();
            }
        });

        builder.show();


    }


}
