package com.example.sadokmm.appmaterial.objects;

import android.graphics.Bitmap;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Commentaire {

    private String emailUser;
    private String userName;
    private String commentaire;
    private Bitmap userPhoto;
    private Date date;


    public Commentaire(String userName, String commentaire, Bitmap userPhoto , String emailUser) {
        this.userName = userName;
        this.commentaire = commentaire;
        this.userPhoto = userPhoto;
        this.emailUser = emailUser;
        this.date = Calendar.getInstance().getTime();
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Bitmap getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(Bitmap userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getEmailUser() {
        return emailUser;
    }

    public void setEmailUser(String emailUser) {
        this.emailUser = emailUser;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStringDate() {
        DateFormat df = new SimpleDateFormat(" d MMM yyyy, HH:mm");
        String date = df.format(this.date);
        return date;
    }
}
