package com.example.sadokmm.appmaterial.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.sadokmm.appmaterial.R;
import com.example.sadokmm.appmaterial.activities.PlanActivity;
import com.example.sadokmm.appmaterial.objects.Plan;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import static com.example.sadokmm.appmaterial.activities.FirstMain.myListPlanFood;
import static com.example.sadokmm.appmaterial.activities.MainActivity.bp;
import static com.example.sadokmm.appmaterial.activities.MainActivity.mp;
import static com.example.sadokmm.appmaterial.activities.FirstMain.myListPlan;

public class AdapterForList extends RecyclerView.Adapter<AdapterForList.ViewHolder> implements Filterable {


    private List<Plan> myListPlanFoodSearch;
    private LayoutInflater layoutInflater;
    Context con;


    public static ArrayList<Plan> getMyListPlanFood() {
        return myListPlanFood;
    }



    public AdapterForList(Context context) {
        layoutInflater=LayoutInflater.from(context);
        myListPlanFoodSearch=new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=layoutInflater.inflate(R.layout.test,viewGroup,false);
        ViewHolder viewHolder=new ViewHolder(view);


        return viewHolder;
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

       final Plan plan=myListPlanFoodSearch.get(i);
        viewHolder.itemTitle.setText(plan.getTitre());
        viewHolder.itemThumbnail.setImageBitmap(plan.getImage());

        if ( plan.getType().equals("BP") ) {
            viewHolder.planType.setImageResource(R.drawable.ic_bp2);
        }

        viewHolder.itemThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(layoutInflater.getContext(),PlanActivity.class);


                int position=0;

                for (int i=0 ; i<myListPlan.size() ; i++){

                    if (myListPlan.get(i)==plan)
                        position=i;

                }


                intent.putExtra("i",position);

                layoutInflater.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return myListPlanFoodSearch.size();
    }

    public void setMyList(ArrayList<Plan> list){
        myListPlanFood=list;
        myListPlanFoodSearch.addAll(myListPlanFood);
        notifyItemRangeChanged(0,list.size());
    }

    @Override
    public Filter getFilter() {
        return exampleFilter;
    }

    private Filter exampleFilter=new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<Plan> filteredFoodList=new ArrayList<>();
            if (charSequence == null || charSequence.length() == 0){
                filteredFoodList.addAll(myListPlanFood);
            } else {
                String filterPattern=charSequence.toString().toLowerCase().trim();

                for (Plan plan : myListPlanFood){
                    if (plan.getTitre().toLowerCase().contains(filterPattern)){
                        filteredFoodList.add(plan);
                    }
                }
            }

            FilterResults results=new FilterResults();
            results.values = filteredFoodList;
            return results;

        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

            myListPlanFoodSearch.clear();
            myListPlanFoodSearch.addAll((List<Plan>)filterResults.values);
            notifyDataSetChanged();
        }
    };




    //

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView itemTitle;
        private TextView itemDesc;
        private ImageView itemThumbnail;
         ImageView planType;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            itemTitle=(TextView)itemView.findViewById(R.id.itemTitre1);
          // itemDesc=(TextView)itemView.findViewById(R.id.itemDesc);
            itemThumbnail=(ImageView)itemView.findViewById(R.id.itemThumbnail1);

            planType = (ImageView)itemView.findViewById(R.id.typePlanItemm);

        }
    }


    public void refresh(){
        myListPlanFoodSearch.clear();
        myListPlanFoodSearch.addAll(myListPlanFood);
        notifyDataSetChanged();
    }

}
